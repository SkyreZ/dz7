function Palindrome (str) {
    return str === str.split('').reverse().join('');
}

function findVowels(str) {
    return str.length;
}

module.exports = Palindrome, findVowels;