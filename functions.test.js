const request = require("request");
const publicIp = require('public-ip');
describe("Palindrome function", () => {
    const Palindrome = require("./functions");

    test("Level", () => {
        expect(Palindrome("level")).toBeTruthy();
    });

    test("Radar", () => {
        expect(Palindrome("radar")).toBeTruthy();
    });

    test("Stats", () => {
        expect(Palindrome("stats")).toBeTruthy();
    });

    test("Tenet", () => {
        expect(Palindrome("tenet")).toBeTruthy();
    });

    test(":Wow", () => {
        expect(Palindrome("Wow")).toBeTruthy();
    });
});
    test("Obj 2", () => {
        expect(arr[0]).toBe(2);
    });

    test("Obj 5", () => {
        expect(arr[1]).toBe("beta");
    });

    test("Obj 3", () => {
        expect(arr[3]).toBe("alpha");
    });

    test("Obj 20", () => {
        expect(arr[6]).toBe("beta");
    });

    test("Obj 15", () => {
        expect(arr[2]).toBe("alphabeta");
    });
});

describe("findVowels function", () => {
    const findVowels = require("./functions");

    test("Diamond length", () => {
        expect(findVowels("Shine on you crazy diamond")).toBe(3);
    });

    test("Echoes length", () => {
        expect(findVowels("echoes")).toBe(6);
    });

    test("Queen", () => {
        expect(findVowels("queen")).toBe(16);
    });

    test("Zeppeliin", () => {
        expect(findVowels("zeppelin")).toBe(5);
    });

    test("Thunder", () => {
        expect(findVowels("thunder")).toBe(7);
    });
});

describe("Ip test", () => {
    test("Ip", async () => {
        request("https://httpbin.org/get", async function(
            error,
            response
        ) {
            let ipv4 = await publicIp.v4({ onlyHttps: true });
            let obj = JSON.parse(response.body);
            expect(response.origin).toBe(ipv4)
            console.log("obj", obj[0].origin);
        });
    });
});